package com.dlgdev.games.larvitarnotifications

import android.app.*
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

class LarvitarNotificatorService : Service() {

    override fun onBind(intent: Intent): IBinder {
        Log.d(SERVICE_NAME, "Service started")
        setupNotification()
        startLocationTracking()
        clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        started = true
        return Binder()
    }

    override fun onUnbind(intent: Intent): Boolean {
        notificationManager.cancelAll()
        return super.onUnbind(intent)
    }

    companion object {
        const val NOTIF_ID = 101
        const val NOTIF_CHANNEL = "Notification channel because reasons"
        const val SERVICE_NAME = "LARVITAR_NOTIFICATION"
        const val EXTRA_PERCENT = "EXTRA_PERCENT"
    }

    var started = false
    lateinit var notificationManager: NotificationManager
    lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var clipboard: ClipboardManager

    /**
     * Will be called upon startup and every time a button is clicked
     * On startup, we set up the notification and prepare the location service to be polled on request
     * On button clicked, we just grab the percentage posted and will use that for the text
     */
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
            Log.d(SERVICE_NAME, "Request received")
            if (intent != null) {
                if (intent.extras != null && intent.extras.containsKey(EXTRA_PERCENT)) {
                    val percent = intent.extras[EXTRA_PERCENT] as Int
                    sharePercent(percent)
                }
            }
        return Service.START_NOT_STICKY
    }

    private fun startLocationTracking() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    private fun setupNotification() {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(NotificationChannel(NOTIF_CHANNEL, SERVICE_NAME,
                    NotificationManager.IMPORTANCE_HIGH))
        }
        notificationManager.notify(NOTIF_ID, notification())
    }

    private fun sharePercent(percent: Int) {
        try {
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if (it != null) {
                    val text = checkClipboardContent(percent, it)
                    clipboard.primaryClip = ClipData.newPlainText(text, text)
                    Toast.makeText(this, getString(R.string.copied_to_clipboard, percent),
                            Toast.LENGTH_SHORT)
                            .show()
                }
            }
        } catch (se: SecurityException) {
            Toast.makeText(this, R.string.enable_location_services,
                    Toast.LENGTH_LONG).show()
            Log.d("Some random", "Decided not to enable location services.")
        }
    }

    private fun checkClipboardContent(percent: Int, location: Location): String {
        val clip = clipboard.primaryClip
        return if (clip.description.label == "CalcyIV rename") {
            "${clip.getItemAt(0).text} - ${getString(R.string.pasteable, percent, location.latitude,
                    location.longitude)}"
        } else {
            getString(R.string.pasteable, percent, location.latitude, location.longitude)
        }
    }

    private fun notification(): Notification =
            NotificationCompat.Builder(this, NOTIF_CHANNEL)
                    .addAction(actionFor(100))
                    .addAction(actionFor(98))
                    .addAction(actionFor(96))
                    .setOngoing(true)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentText(getString(R.string.click_on_action))
                    .build()

    private fun actionFor(percent: Int): NotificationCompat.Action =
            NotificationCompat.Action.Builder(
                    R.drawable.ic_launcher_background,
                    getString(R.string.found, percent),
                    pendingIntent(percent))
                    .build()

    private fun pendingIntent(percent: Int): PendingIntent {
        val intent = Intent(this, LarvitarNotificatorService::class.java)
        intent.putExtra(EXTRA_PERCENT, percent)
        return PendingIntent.getService(this, percent, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }
}
